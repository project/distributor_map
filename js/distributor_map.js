/**
 * @file
 * It contains the basic function for creating distributor map.
 */

(function($){
  var realWidth,realHeight;
  $(document).ready(function() {
    $("#dismap").click(function(event) {
      var elOffsetX = $(this).offset().left,
      elOffsetY = $(this).offset().top,
      clickOffsetX = event.pageX - elOffsetX,
      clickOffsetY = event.pageY - elOffsetY;
	  var img = $("img.mappoint");	  
	  $("<img>").attr("src", $(img).attr("src")).load(function() {
        realWidth = this.width;
        realHeight = this.height;       
		event.preventDefault();	    
        $("#edit-field-dis-xy-und-0-value").val(clickOffsetX + "," + parseInt(clickOffsetY));
        $(".mappoint").css("margin-left",clickOffsetX - (realWidth/2));
        $(".mappoint").css("margin-top",clickOffsetY - (realHeight));
      });      
    });
  });
})(jQuery); 
